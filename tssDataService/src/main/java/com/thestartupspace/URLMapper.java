package com.thestartupspace;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thestartupspace.datastore.DBAccessor;
import com.thestartupspace.datastore.DatastoreAccessor;
import com.thestartupspace.models.User;

@RestController
public class URLMapper {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private DatastoreAccessor dataStoreAccessor = new DBAccessor();

    @RequestMapping("o")
    public Library greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Library(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping("/getUserByEmail")
    public User getUserByEmail(@RequestParam(value="email", defaultValue="") String email) {
    	if (email == "") {
    		return User.NULL_USER;
    	}
    	
    	return dataStoreAccessor.getUserByEmail(email);
    }
}