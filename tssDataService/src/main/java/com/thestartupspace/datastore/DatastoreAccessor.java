package com.thestartupspace.datastore;

import com.thestartupspace.models.User;

public interface DatastoreAccessor {
	public User getUserByEmail(String email);
}
