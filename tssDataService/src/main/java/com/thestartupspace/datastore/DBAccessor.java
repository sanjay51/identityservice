package com.thestartupspace.datastore;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.thestartupspace.Application;
import com.thestartupspace.models.User;

public class DBAccessor implements DatastoreAccessor {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return new User(load());
	}

    JdbcTemplate jdbcTemplate = Application.jdbcTemplate;
    
    public String load() {
    	log.info("Loading from DB");
    	log.info(Application.jdbcTemplate == null ? "TRUE" : Application.jdbcTemplate.toString());
    	return Application.jdbcTemplate.query("Select * from users", mapper).get(0);
    	
    }
    
	private static final RowMapper<String> mapper = new RowMapper<String>() {
		
		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString("name");
		}
    };

}
