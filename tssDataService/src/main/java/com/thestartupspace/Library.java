package com.thestartupspace;

public class Library {

    private final long id;
    private final String content;

    public Library(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}