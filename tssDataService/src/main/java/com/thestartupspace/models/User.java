package com.thestartupspace.models;

public class User {
	public static User NULL_USER = new User();
	private String displayName;
	
	public User() {
		this.displayName = "";
	}
	
	public User(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
